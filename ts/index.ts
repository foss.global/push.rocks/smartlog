import * as plugins from './smartlog.plugins.js';
import { ConsoleLog } from './smartlog.classes.consolelog.js';
import { LogGroup } from './smartlog.classes.loggroup.js';
import { Smartlog } from './smartlog.classes.smartlog.js';

export { ConsoleLog, LogGroup, Smartlog };
