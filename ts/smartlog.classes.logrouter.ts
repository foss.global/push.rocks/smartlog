import * as plugins from './smartlog.plugins.js';

export class LogRouter {
  /**
   * all log destinations
   */
  private logDestinations: plugins.smartlogInterfaces.ILogDestination[] = [];

  constructor() {}

  public addLogDestination(logDestination: plugins.smartlogInterfaces.ILogDestination) {
    this.logDestinations.push(logDestination);
  }

  // routes the log according to added logDestinations
  public async routeLog(logPackageArg: plugins.smartlogInterfaces.ILogPackage) {
    for (const logDestination of this.logDestinations) {
      await logDestination.handleLog(logPackageArg);
    }
  }
}
